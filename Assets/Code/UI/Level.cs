﻿using System;
using UnityEngine;

public class Level : MonoBehaviour, ISwitchable {
    [SerializeField] int threeStars = 1;
    [SerializeField] int twoStars = 3;
    [SerializeField] int maxMoves = 10;
    [SerializeField] LevelFader levelFader;

    int moves;

    public int Moves {
        get { return moves; }
        set {
            moves = value;
            UpdateMoves(moves);
        }
    }

    public Action<int> UpdateMoves = delegate { };
    public Action<int> RemoveStar = delegate { };

    public int levelId { get; set; }
    public string levelName { get; set; }

    public int AchievedStars { get; set; }

    void Awake() {
        levelName = GameMaster.LevelList[levelId];
        AchievedStars = 3;
    }

    public int GetLevelProgress() {
        return PlayerPrefs.GetInt(levelName);
    }

    public void SaveLevelProgress() {
        if (PlayerPrefs.GetInt(levelName) >= AchievedStars) {
            return;
        }
        PlayerPrefs.SetInt(levelName, AchievedStars);
    }

    public void Open() {
        levelFader.StartFadeIn();
    }

    public void Close() {
        Destroy(gameObject);
    }

    public void CheckLevelProgress() {
        if (Moves >= maxMoves) {
            RemoveStar(1);
            AchievedStars = 0;
            GameMaster.ScreenSwitcher.ShowGameOverScreen();
        }
        else if (Moves >= twoStars) {
            RemoveStar(2);
            AchievedStars = 1;
        }
        else if (Moves >= threeStars) {
            RemoveStar(3);
            AchievedStars = 2;
        }
        else {
            AchievedStars = 3;
        }
    }
}