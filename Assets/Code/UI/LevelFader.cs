﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelFader : MonoBehaviour {
    public Image img;
    public AnimationCurve curve;

    public void StartFadeIn() {
        StartCoroutine(FadeIn());
    }

    public void StartFadeOut(GameObject scene) {
        StartCoroutine(FadeOut(scene));
    }

    IEnumerator FadeIn() {
        float t = 1f;

        while (t > 0f) {
            t -= Time.deltaTime;
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }
    }

    IEnumerator FadeOut(GameObject levelObject) {
        float t = 0f;

        while (t < 1f) {
            t += Time.deltaTime;
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }
        Destroy(levelObject);
    }
}