﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonBehaviour : MonoBehaviour {
    [SerializeField] TMP_Text lvlNumber;
    [SerializeField] Image lockedImage;
    [SerializeField] Button levelButton;
    [SerializeField] List<Transform> starIcons = new List<Transform>();


    public void Setup(string levelName, int number, bool locked) {
        lockedImage.gameObject.ChangeActive(locked);
        lvlNumber.gameObject.ChangeActive(locked == false);
        levelButton.interactable = locked == false;
        lvlNumber.text = number.ToString();
        levelButton.onClick.AddListener(() => ShowLevel(number));
        if (locked) {
            HideStars(levelName);
        }
        else {
            SetupStars(levelName);
        }
    }

    void HideStars(string levelName) {
        PlayerPrefs.SetInt(levelName, 0);
        for (int index = 0; index < starIcons.Count; index++) {
            starIcons[index].gameObject.Deactivate();
        }
    }

    void SetupStars(string levelName) {
        int achievedStars = PlayerPrefs.GetInt(levelName);
        for (int index = 0; index < starIcons.Count; index++) {
            if (index < achievedStars) {
                starIcons[index].gameObject.Activate();
            }
            else {
                starIcons[index].gameObject.Deactivate();
            }
        }
    }

    void ShowLevel(int number) {
        GameMaster.Levels.Show(number);
    }
}