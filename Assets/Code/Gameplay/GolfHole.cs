﻿using UnityEngine;

public class GolfHole : MonoBehaviour {
    [SerializeField] float holeDepth;
    [SerializeField] float minSpeed;
    
    bool inHole;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.layer != LayerMask.NameToLayer("Player")) {
            return;
        }
        GolfBall ball = collider.gameObject.GetComponent<GolfBall>();

        if (ball == null) {
            Debug.Log("This is not ball.");
            return;
        }
        if (ball.BallVelocity < minSpeed) {
            ball.InHole(transform.position);
            inHole = true;
        }
    }

    void OnTriggerStay2D(Collider2D collider) {
        if (collider.gameObject.layer != LayerMask.NameToLayer("Player")) {
            return;
        }

        if (inHole) {
            return;
        }

        GolfBall ball = collider.gameObject.GetComponent<GolfBall>();

        if (ball == null) {
            Debug.Log("This is not ball.");
            return;
        }
        float distance = Vector2.Distance(ball.transform.position, transform.position);

        Vector3 force = (ball.transform.position - transform.position) * holeDepth / distance;
        ball.AddForce(force);
    }
}