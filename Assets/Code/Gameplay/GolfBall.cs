﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class GolfBall : MonoBehaviour {
    const float MIN_SIGHT_SCALE = 0.5f;
    const float MAX_SIGHT_SCALE = 2f;
    const float SIGHT_SCALE_FACTOR = 0.01f;
    const float TAP_OFFSET = 100;
    const float MIN_BALL_SPEED = 0.15f;
    const int SPIN_FACTOR = 7;
    const int LERP_FACTOR = 2;

    [SerializeField] float speed;
    [SerializeField] GameObject sight;
    [SerializeField] GameObject trail;
    [SerializeField] AnimationCurve animationCurve;
    [SerializeField] float animationTime = 3f;

    Swipe swipeController;
    Rigidbody2D rbody2D;
    SpriteRenderer ballSprite;

    Vector2 startPosition;
    Vector2 endPosition;
    Vector2 direction;

    float scale;

    public bool inHole;


    bool isMoving {
        get { return BallVelocity > MIN_BALL_SPEED; }
    }

    public float BallVelocity {
        get { return rbody2D.velocity.magnitude; }
    }

    #region unity_methods

    void Awake() {
        Initialize();
    }

    void FixedUpdate() {
        EndMove();
    }

    void OnDestroy() {
        RemoveListeners();
    }

    #endregion

    void Initialize() {
        rbody2D = GetComponent<Rigidbody2D>();
        ballSprite = GetComponent<SpriteRenderer>();
        swipeController = GetComponent<Swipe>();
        if (swipeController == null) {
            swipeController = gameObject.AddComponent<Swipe>();
        }
        AddListeners();
        sight.Deactivate();
    }

    void AddListeners() {
        swipeController.OnTapDown += OnBeginDrag;
        swipeController.OnDrag += OnDrag;
        swipeController.OnTapUp += OnEndDrag;
    }

    void RemoveListeners() {
        if (swipeController == null) {
            return;
        }
        swipeController.OnTapDown -= OnBeginDrag;
        swipeController.OnDrag -= OnDrag;
        swipeController.OnTapUp -= OnEndDrag;
    }

    public void OnBeginDrag() {
        if (isMoving || inHole) {
            return;
        }
        StartDrag();
    }

    public void OnDrag() {
        if (isMoving || inHole) {
            return;
        }
        EvaluateDrag();
    }

    public void OnEndDrag() {
        if (isMoving || inHole) {
            return;
        }
        ReleaseDrag();
    }

    void EndMove() {
        if (BallVelocity > 0 && isMoving == false && inHole == false) {
            StopBall();
            GameMaster.CurrentLevel.CheckLevelProgress();
        }
    }

    public void StopBall() {
        trail.Deactivate();
        rbody2D.velocity = Vector2.zero;
    }

    void StartDrag() {
        sight.Activate();
        sight.transform.localScale = Vector3.zero;
        startPosition = swipeController.LastTouch;
    }

    void EvaluateDrag() {
        endPosition = swipeController.LastTouch;
        direction = endPosition - startPosition;
        sight.transform.rotation = Quaternion.Euler(SwipeAngle());
        scale = Mathf.Clamp((direction.magnitude - TAP_OFFSET) * SIGHT_SCALE_FACTOR, 0,
            MAX_SIGHT_SCALE);
        if (scale < MAX_SIGHT_SCALE) {
            sight.transform.localScale = (scale + MIN_SIGHT_SCALE) * Vector2.one;
        }
    }

    void ReleaseDrag() {
        if (scale > 0) {
            AddForce(-direction.normalized * speed * scale);
            GameMaster.CurrentLevel.Moves++;
        }
        trail.Activate();
        sight.Deactivate();
    }

    public Vector3 SwipeAngle() {
        int sign = endPosition.x < startPosition.x ? 1 : -1;
        float angle = 180 + Vector2.Angle(Vector2.up, direction) * sign;
        return Vector3.forward * angle;
    }


    public void InHole(Vector3 hole) {
        inHole = true;
        StopBall();
        StartCoroutine(AnimateBallInHole(hole));
    }

    IEnumerator AnimateBallInHole(Vector3 hole) {
        float time = 0;
        while (time < 1f) {
            time += Time.deltaTime / animationTime;
            float curve = animationCurve.Evaluate(time);
            transform.localScale = Vector2.one * curve;
            ballSprite.color = Color.white * curve;
            transform.RotateAround(hole, Vector3.forward, SPIN_FACTOR * curve);
            transform.position = Vector3.Lerp(transform.position, hole, LERP_FACTOR * Time.deltaTime);
            yield return null;
        }
        GameMaster.ScreenSwitcher.ShowVictoryScreen();
        GameMaster.CurrentLevel.SaveLevelProgress();
    }

    public void AddForce(Vector3 force) {
        rbody2D.AddForce(force);
    }
}