﻿using UnityEngine;

public class Hill : MonoBehaviour {
    [SerializeField] protected float depth;
    [SerializeField] protected float minDistance;

    public virtual void OnTriggerStay2D(Collider2D collider) {
        if (collider.gameObject.layer != LayerMask.NameToLayer("Player")) {
            return;
        }

        GolfBall ball = collider.gameObject.GetComponent<GolfBall>();
        if (ball == null) {
            Debug.Log("This is not ball.");
            return;
        }
        if (ball.inHole) {
            return;
        }
        float distance = Vector2.Distance(ball.transform.position, transform.position);
        if (distance < minDistance) {
            return;
        }

        Vector3 force = (ball.transform.position - transform.position) * depth / distance;
        ball.AddForce(force);
    }
}