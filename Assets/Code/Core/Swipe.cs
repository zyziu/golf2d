﻿using System;
using UnityEngine;


public class Swipe : MonoBehaviour {
    public static Swipe Instance;

    Vector2 startTouch, swipeDelta, moveDelta, lastTouch;

    bool isDraging;

    public Vector2 LastTouch {
        get { return lastTouch; }
    }

    public Vector2 SwipeDelta {
        get { return swipeDelta; }
    }

    public Vector2 MoveDelta {
        get { return moveDelta; }
    }

    public Action SwipeLeft = delegate { };
    public Action SwipeRight = delegate { };
    public Action SwipeUp = delegate { };
    public Action SwipeDown = delegate { };

    public Action OnTapDown = delegate { };
    public Action OnTap = delegate { };
    public Action OnDrag = delegate { };
    public Action OnDrop = delegate { };
    public Action OnTapUp = delegate { };

    void Update() {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButtonDown(0)) {
            isDraging = true;
            startTouch = Input.mousePosition;
            lastTouch = startTouch;
            OnTapDown();
        }

        if (isDraging) {
            if (Input.GetMouseButton(0)) {
                swipeDelta = (Vector2) Input.mousePosition - startTouch;
                lastTouch = Input.mousePosition;
                OnDrag();
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            OnDrop();
            OnTapUp();
            Reset();
        }
#elif UNITY_IOS || UNITY_ANDROID
        if (Input.touches.Length > 0) {
            if (Input.touches[0].phase == TouchPhase.Began) {
                isDraging = true;
                startTouch = Input.touches[0].position;
                lastTouch = startTouch;
                OnTapDown();
            }

            if (isDraging) {
                swipeDelta = Input.touches[0].position - startTouch;
                lastTouch = Input.mousePosition;
                OnDrag();
            }

            if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) {
                OnDrop();
                OnTapUp();
                Reset();
            }
        }
#endif
    }


    void Reset() {
        startTouch = swipeDelta = moveDelta = lastTouch = Vector2.zero;
        isDraging = false;
    }
}