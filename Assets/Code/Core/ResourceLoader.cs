﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class ResourceLoader {
    const string META_POSTFIX = ".meta";

    public static T Load<T>(string path) where T : Object {
        Object loadedObject = Resources.Load(path, typeof(T));
        if (loadedObject == null || loadedObject.GetType() != typeof(T)) {
            Debug.Log("Cannot find asset with name: " + path);
            return default(T);
        }
        return (T) loadedObject;
    }

    public static HashSet<string> GetFilesInDirectory(string directory) {
        HashSet<string> allFiles = new HashSet<string>();
#if UNITY_EDITOR
        string dir = Application.dataPath + "/Resources/" + directory;

        if (dir == null || Directory.Exists(dir) == false) {
            return new HashSet<string>();
        }

        allFiles = new HashSet<string>(Directory.GetFiles(dir));

        HashSet<string> files = new HashSet<string>();
        foreach (string file in allFiles) {
            if (Path.GetDirectoryName(file) == dir && file.EndsWith(META_POSTFIX) == false ) {
                files.Add(Path.GetFileNameWithoutExtension(file));
            }
        }
        return files;
#else
        var filesInDir = Resources.LoadAll<Level>(directory);
        foreach (var level in filesInDir) {
            allFiles.Add(level.name);
        }
        Resources.UnloadUnusedAssets();
        return allFiles;
#endif
    }
}