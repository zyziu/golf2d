﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameMaster {
    public const string SCREENS_PREFIX = "Screens";
    public const string LEVEL_PREFIX = "Levels";

    static bool instanceDestroyed;

    public Action OnPlayerMove = delegate { };

    static GameMaster instance;
    public static GameMaster Instance {
        get {
            if (instance != null) {
                return instance;
            }

#if UNITY_EDITOR
            if (Application.isPlaying == false) {
                return null;
            }
#endif
            if (instanceDestroyed == false) {
                instance = new GameMaster();
                return instance;
            }
            return null;
        }
    }

    ScreenSwitcher screenSwitcher;
    public static ScreenSwitcher ScreenSwitcher {
        get { return Instance.screenSwitcher; }
        set { Instance.screenSwitcher = value; }
    }

    LevelManager levels;
    public static LevelManager Levels {
        get { return Instance.levels; }
        set { Instance.levels = value; }
    }

    List<string> levelList;
    public static List<string> LevelList {
        get { return Instance.levelList; }
        set { Instance.levelList = value; }
    }


    public static Level CurrentLevel {
        get { return Instance.levels.GetCurrentLevel(); }
    }

    public GameMaster() {
        instance = this;
        levelList = ResourceLoader.GetFilesInDirectory(LEVEL_PREFIX).ToList();
    }

    ~GameMaster() {
        instanceDestroyed = true;
    }

    public static bool IsInstanceCreated() {
        return instance != null;
    }
}