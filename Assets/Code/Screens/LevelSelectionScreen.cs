﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionScreen : GenericScreen {
    [SerializeField] LevelButtonBehaviour levelButtonBehaviourPrefab;
    [SerializeField] Button backToMenuButton;
    [SerializeField] Transform content;


    void Start() {
        backToMenuButton.onClick.AddListener(BackToMenu);
        int levelReached = PlayerPrefs.GetInt("levelReached", 0);
        for (int index = 0; index < GameMaster.LevelList.Count; index++) {
            string name = GameMaster.LevelList[index];
            LevelButtonBehaviour levelButtonsBehaviour =
                Instantiate(levelButtonBehaviourPrefab, content.position, Quaternion.identity);
            levelButtonsBehaviour.transform.SetParent(content);
            bool locked = index > levelReached;
            levelButtonsBehaviour.Setup(name, index, locked);
        }
    }

    void OnDestroy() {
        if (backToMenuButton != null) {
            backToMenuButton.onClick.RemoveAllListeners();
        }
    }

    void BackToMenu() {
        GameMaster.ScreenSwitcher.ShowMainScreen();
    }
}