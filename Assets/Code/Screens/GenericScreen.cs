﻿using System.Collections;
using UnityEngine;

public abstract class GenericScreen : MonoBehaviour, ISwitchable {
    const string TRANSITION_OPEN_NAME = "IsOpen";
    const string CLOSED_STATE_NAME = "Close";

    [SerializeField] float animationTime;
    [SerializeField] AnimationCurve animationCurve;

    Animator animator;

    public virtual void Awake() {
        animator = GetComponent<Animator>();
    }

    public void Open() {
        if (animator == null) {
            return;
        }
        animator.SetBool(TRANSITION_OPEN_NAME, true);
    }

    public void Close() {
        if (animator == null) {
            return;
        }
        animator.SetBool(TRANSITION_OPEN_NAME, false);
        StartCoroutine(DestroyScreen());
    }

    IEnumerator DestroyScreen() {
        do {
            yield return null;
        } while (!IsCloseAnimationFinished());
        Destroy(gameObject);
    }

    public void OnBackClick() {
        GameMaster.ScreenSwitcher.SwitchToPreviousScreen();
    }

    public bool IsCloseAnimationFinished() {
        return animator.GetCurrentAnimatorStateInfo(0).IsName(CLOSED_STATE_NAME);
    }

    protected IEnumerator AnimateScaleUp(Transform target) {
        float time = 0f;
        while (time < 1f) {
            time += Time.deltaTime / animationTime;
            float curve = animationCurve.Evaluate(time);
            target.localScale = Vector3.one * curve;
            yield return null;
        }
    }

    protected IEnumerator AnimateScaleDown(Transform target) {
        float time = 1f;

        while (time > 0f) {
            time -= Time.deltaTime / animationTime;
            float curve = animationCurve.Evaluate(time);
            target.localScale = Vector3.one * curve;
            yield return null;
        }
        target.gameObject.Deactivate();
    }
}