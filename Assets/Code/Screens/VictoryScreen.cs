﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : GenericScreen {
    [SerializeField] Button nextLevelButton;
    [SerializeField] Button retryLevelButton;
    [SerializeField] Button levelSelectionButton;
    [SerializeField] List<Transform> starsIcons;
    [SerializeField] TMP_Text score;

    public override void Awake() {
        AddListeners();
        Setup();
        UnlockNextLevel();
        base.Awake();
    }

    void OnDestroy() {
        RemoveListeners();
    }

    void AddListeners() {
        nextLevelButton.onClick.AddListener(ShowNextLevel);
        retryLevelButton.onClick.AddListener(RetryCurrentLevel);
        levelSelectionButton.onClick.AddListener(ShowLevelMenu);
    }

    void Setup() {
        
        score.text = GameMaster.CurrentLevel.Moves.ToString();
        SetupStars();
        }

    static void UnlockNextLevel() {
        int unlockedLevels = PlayerPrefs.GetInt("levelReached", 0);
        if (unlockedLevels != GameMaster.CurrentLevel.levelId) {
            return;
        }
        unlockedLevels++;
        if (unlockedLevels < GameMaster.LevelList.Count) {
            PlayerPrefs.SetInt("levelReached", unlockedLevels);
        }
    }

    void SetupStars() {
        int achievedStars = GameMaster.CurrentLevel.AchievedStars;
        for (int index = 0; index < starsIcons.Count; index++) {
            if (index < achievedStars) {
                starsIcons[index].gameObject.Activate();
                StartCoroutine(AnimateScaleUp(starsIcons[index]));
            }
            else {
                starsIcons[index].gameObject.Deactivate();
            }
        }
    }

    void RemoveListeners() {
        if (nextLevelButton) {
            nextLevelButton.onClick.RemoveAllListeners();
        }
        if (retryLevelButton) {
            retryLevelButton.onClick.RemoveAllListeners();
        }
        if (levelSelectionButton) {
            levelSelectionButton.onClick.RemoveAllListeners();
        }
    }

    void RetryCurrentLevel() {
        GameMaster.Levels.RestartLevel();
    }

    void ShowNextLevel() {
        GameMaster.Levels.ShowNextLevel();
    }

    void ShowLevelMenu() {
        GameMaster.Levels.CloseLevel();
        GameMaster.ScreenSwitcher.ShowLevelScreen();
    }
}