﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : GenericScreen {
    [SerializeField] Button retryLevelButton;
    [SerializeField] Button levelSelectionButton;

    public override void Awake() {
        retryLevelButton.onClick.AddListener(RetryCurrentLevel);
        levelSelectionButton.onClick.AddListener(ShowLevelMenu);
        base.Awake();
    }

    void OnDestroy() {
        if (retryLevelButton) {
            retryLevelButton.onClick.RemoveAllListeners();
        }
        if (levelSelectionButton) {
            levelSelectionButton.onClick.RemoveAllListeners();
        }
    }

    void RetryCurrentLevel() {
        GameMaster.Levels.RestartLevel();
    }

    void ShowLevelMenu() {
        GameMaster.Levels.CloseLevel();
        GameMaster.ScreenSwitcher.ShowLevelScreen();
    }
}