﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayScreen : GenericScreen {
    [SerializeField] Button restartButton;
    [SerializeField] Button mainMenuButton;
    [SerializeField] TMP_Text movesLabel;
    [SerializeField] List<Transform> starIcons;

    public override void Awake() {
        AddListeners();
        Reset();
        base.Awake();
    }
    
    public void OnDestroy() {
        RemoveListeners();
    }
    
    void AddListeners() {
        restartButton.onClick.AddListener(RestartLevel);
        mainMenuButton.onClick.AddListener(CloseGameplayScreen);
        GameMaster.CurrentLevel.UpdateMoves += UpdateScore;
        GameMaster.CurrentLevel.RemoveStar += RemoveStar;
    }

    void RemoveListeners() {
        if (GameMaster.Instance != null && GameMaster.CurrentLevel != null) {
            GameMaster.CurrentLevel.UpdateMoves -= UpdateScore;
            GameMaster.CurrentLevel.RemoveStar -= RemoveStar;
        }
        if (restartButton) {
            restartButton.onClick.RemoveAllListeners();
        }
        if (mainMenuButton) {
            mainMenuButton.onClick.RemoveAllListeners();
        }
    }

    void RemoveStar(int index) {
        StartCoroutine(AnimateScaleDown(starIcons[index-1]));
    }

    void ResetStars() {
        for (int i = 0; i < starIcons.Count; ++i) {
            starIcons[i].gameObject.Activate();
            StartCoroutine(AnimateScaleUp(starIcons[i]));
        }
    }

    void UpdateScore(int stars) {
        movesLabel.text = GameMaster.CurrentLevel.Moves.ToString();
    }

    void Reset() {
        StopAllCoroutines();
        ResetStars();
        movesLabel.text = "0";
    }

    void RestartLevel() {
        GameMaster.Levels.RestartLevel();
        Reset();
    }

    void CloseGameplayScreen() {
        GameMaster.ScreenSwitcher.ShowMainScreen();
        GameMaster.Levels.CloseLevel();
    }
}