﻿public class LevelManager : Switcher {
    int currentLevelId;

    void Awake() {
        GameMaster.Levels = this;
    }

    public virtual void Show(int levelId) {
        if (levelId > GameMaster.LevelList.Count) {
            return;
        }

        string levelName = GameMaster.LevelList[levelId];

        base.Show(GameMaster.LEVEL_PREFIX + "/" + levelName);

        currentLevelId = levelId;

        Level lvl = (Level) current;
        lvl.levelName = levelName;
        lvl.levelId = levelId; 

        GameMaster.ScreenSwitcher.ShowGameplayScreen();
    }

    public void RestartLevel() {
        Show(currentLevelId);
    }

    public void ShowNextLevel() {
        Show(++currentLevelId);
    }

    public void CloseLevel() {
        CloseCurrent();
        current = null;
    }

    public Level GetCurrentLevel() {
        return (Level) current;
    }
}