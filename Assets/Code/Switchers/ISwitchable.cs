﻿public interface ISwitchable {
    void Open();
    void Close();

}