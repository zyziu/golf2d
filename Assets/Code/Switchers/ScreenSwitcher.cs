﻿using System.Collections.Generic;

public class ScreenSwitcher : Switcher {
    public enum ScreenType {
        Unknown,
        Loading,
        Main,
        LevelSelection,
        Game,
        Victory,
        GameOver
    }

    readonly Dictionary<ScreenType, string> screenAssetNames = new Dictionary<ScreenType, string>() {
        {ScreenType.Loading, "LoadingScreen"},
        {ScreenType.Main, "MainScreen"},
        {ScreenType.LevelSelection, "LevelSelectionScreen"},
        {ScreenType.Game, "GameplayScreen" },
        {ScreenType.Victory, "VictoryScreen"},
        {ScreenType.GameOver, "GameOverScreen"}
    };

    ScreenType currentScreenType = ScreenType.Unknown;
    ScreenType previousScreenType = ScreenType.Unknown;

    void Awake() {
        GameMaster.ScreenSwitcher = this;
    }

    void Start() {
        Show(ScreenType.Main);
    }

    public virtual void Show(ScreenType screen) {
        if (screen == currentScreenType) {
            ClearScreen();
        }
        if (screenAssetNames.ContainsKey(screen) == false) {
            return;
        }
        
        string screenName = screenAssetNames[screen];
        base.Show(GameMaster.SCREENS_PREFIX + "/" + screenName);

        previousScreenType = currentScreenType;
        currentScreenType = screen;
    }

    void ClearScreen() {
        GenericScreen currentScreen = (GenericScreen) current;
        current = null;
        Destroy(currentScreen.gameObject);
    }

    public void SwitchToPreviousScreen() {
        if (previousScreenType == ScreenType.Unknown) {
            return;
        }
        Show(previousScreenType);
    }

    public void ShowMainScreen() {
        Show(ScreenType.Main);
    }

    public void ShowLevelScreen() {
        Show(ScreenType.LevelSelection);
    }

    public void ShowVictoryScreen() {
        Show(ScreenType.Victory);
    }

    public void ShowGameOverScreen() {
        Show(ScreenType.GameOver);
    }

    public void ShowGameplayScreen() {
        Show(ScreenType.Game);
    }
}