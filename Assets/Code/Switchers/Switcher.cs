﻿using UnityEngine;

public abstract class Switcher : MonoBehaviour {
    protected ISwitchable current;

    public virtual void Show(string objectPath) {
        GameObject loadedScreen = ResourceLoader.Load<GameObject>(objectPath);

        if (loadedScreen == null) {
            return;
        }

        ISwitchable nextScreen = Instantiate<ISwitchable>(loadedScreen);
        Open(nextScreen);
    }

    protected T Instantiate<T>(GameObject template) {
        if (template == null) {
            return default(T);
        }

        GameObject objectInstace = Instantiate(template, transform, false);
        if (objectInstace == null) {
            Debug.Log("Spawned object is null.");
            return default(T);
        }
        objectInstace.gameObject.Activate();
        objectInstace.transform.SetAsLastSibling();

        T targetObject = objectInstace.GetComponent<T>();
        if (targetObject == null) {
            Debug.Log("There is no target script attached.");
            return default(T);
        }

        return targetObject;
    }

    protected void Open(ISwitchable nextObject) {
        CloseCurrent();
        current = nextObject;
        nextObject.Open();
    }

    protected void CloseCurrent() {
        if (current == null) {
            return;
        }
        current.Close();
    }
}