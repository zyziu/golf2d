﻿using UnityEngine;

public static class GameObjectExtensions {
    public static bool IsDestroyed(this object obj) {
        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
        return obj == null && !ReferenceEquals(obj, null);
    }

    public static void Deactivate(this GameObject go) {
        if (go == null || go.IsDestroyed()) {
            return;
        }

        if (go.activeSelf) {
            go.SetActive(false);
        }
    }

    public static void Activate(this GameObject go) {
        if (go == null || go.IsDestroyed()) {
            return;
        }

        if (go.activeSelf == false) {
            go.SetActive(true);
        }
    }

    public static void ChangeActive(this GameObject go, bool activate) {
        if (go == null || go.IsDestroyed()) {
            return;
        }

        if (activate) {
            go.Activate();
        }
        else {
            go.Deactivate();
        }
    }
}