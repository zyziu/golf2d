﻿using UnityEngine;

namespace Assets.Code {
    public static class UnityObjectExtensions {
        public static bool Destroyed(this MonoBehaviour engineObject) {
            return engineObject == null ||
                   engineObject.IsDestroyed() ||
                   engineObject.transform == null ||
                   engineObject.transform.IsDestroyed() ||
                   // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                   engineObject.gameObject == null ||
                   engineObject.gameObject.IsDestroyed();
        }
    }
}